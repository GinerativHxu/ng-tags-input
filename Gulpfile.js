'use strict';

const conf = require('./gulp/conf.js');
const gulp = require('gulp');

require('load-gulp-tasks')(
    gulp, {
        pattern: ['gulp/tasks/*.js'],
        config: conf
    }, {
        uglify: require('gulp-uglify'),
        sass: require('gulp-sass'),
        ngTemplates: require('gulp-ng-templates'),
        replace: require('gulp-replace'),
        file: require('gulp-file'),
        del: require('del'),
        add: require('gulp-add-src'),
        annotate: require('gulp-ng-annotate'),
        concat: require('gulp-concat')
    }
);

gulp.task('default', ['javascript', 'sass', 'sass-boostrap', 'templates', 'clean']);
gulp.task('last', ['templates']);


