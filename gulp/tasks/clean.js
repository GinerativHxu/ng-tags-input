'use strict';

const path = require('path');

module.exports = (gulp, options, plugins) => {

    gulp.task('clean', ['last'], (cb) => {
        const templatesOut = path.parse(options.config.files.html.out);

        plugins.del([
               options.config.files.html.out
            ], {
                force: true
            });
        cb();
    });

};