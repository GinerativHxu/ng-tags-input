'use strict';

const path = require('path');

module.exports = (gulp, options, plugins) => {

    gulp.task('templates', (cb) => {
        const out = path.parse(options.config.files.html.out);

        gulp.src(options.config.files.html.src).
            pipe(plugins.ngTemplates({
                filename: out.base,
                module: 'tagsInput',
                path: (filepath, base) => {
                    const templatePath = path.parse(filepath);
                    return 'ngTagsInput/' + templatePath.base
                },
                header: '<%= module %>.run(["$templateCache", function($templateCache) {'
            })).
            pipe(gulp.dest(out.dir));
        cb();
    });
};