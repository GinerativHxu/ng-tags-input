'use strict';

const path = require('path');

module.exports = (gulp, options, plugins) => {
    const out = path.parse(options.config.files.js.out);

    gulp.task('javascript', ['templates'], (cb) => {
        gulp.src(options.config.files.js.src).
            pipe(plugins.replace(/('|")use strict('|");/, '')).
            pipe(plugins.file('use-strict.js', "'use strict';")).
            pipe(plugins.annotate()).
            pipe(plugins.add.append(options.config.files.html.out)).
            pipe(plugins.concat(out.base)).
            pipe(gulp.dest(out.dir));

        gulp.src(options.config.files.js.src).
            pipe(plugins.replace(/('|")use strict('|");/, '')).
            pipe(plugins.file('use-strict.js', "'use strict';")).
            pipe(plugins.annotate()).
            pipe(plugins.add.append(options.config.files.html.out)).
            pipe(plugins.concat(((filename, ext) => filename + '.min' + ext)(out.name, out.ext))).
            pipe(plugins.uglify({
                mangle: true,
                compress: true
            })).
            pipe(gulp.dest(out.dir));
        cb();
    });
};