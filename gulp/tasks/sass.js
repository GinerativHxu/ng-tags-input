'use strict';

const path = require('path');

module.exports = (gulp, options, plugins) => {

    gulp.task('sass', (cb) => {
        const out = path.parse(options.config.files.css.main.out);
        const outMin = path.parse(options.config.files.css.main.outMin);

        gulp.src(options.config.files.css.main.src).
            pipe(plugins.sass()).
            pipe(plugins.concat(out.base)).
            pipe(gulp.dest(out.dir));

        gulp.src(options.config.files.css.main.src).
            pipe(plugins.sass({
                outputStyle: 'compressed'
            })).
            pipe(plugins.concat(outMin.base)).
            pipe(gulp.dest(outMin.dir));

        cb();
    });

    gulp.task('sass-boostrap', (cb) => {
        const out = path.parse(options.config.files.css.bootstrap.out);
        const outMin = path.parse(options.config.files.css.bootstrap.outMin);

        gulp.src(options.config.files.css.bootstrap.src).
            pipe(plugins.sass()).
            pipe(plugins.concat(out.base)).
            pipe(gulp.dest(out.dir));

        gulp.src(options.config.files.css.bootstrap.src).
            pipe(plugins.sass({
                outputStyle: 'compressed'
            })).
            pipe(plugins.concat(outMin.base)).
            pipe(gulp.dest(outMin.dir));

        cb();
    });
};