'use strict';

module.exports = {
    files: {
        js: {
            src: [
                'src/constants.js',
                'src/init.js',
                'src/tags-input.js',
                'src/tag-item.js',
                'src/auto-complete.js',
                'src/auto-complete-match.js',
                'src/transclude-append.js',
                'src/autosize.js',
                'src/bind-attrs.js',
                'src/configuration.js',
                'src/util.js'
            ],
            out: 'build/ng-tags-input.js',
            outMin: 'build/ng-tags-input.min.js'
        },
        css: {
            main: {
                src: 'scss/main.scss',
                out: 'build/ng-tags-input.css',
                outMin: 'build/ng-tags-input.min.css'
            },
            bootstrap: {
                src: 'scss/bootstrap/main.scss',
                out: 'build/ng-tags-input.bootstrap.css',
                outMin: 'build/ng-tags-input.bootstrap.min.css'
            }
        },
        html: {
            src: [
                'templates/tags-input.html',
                'templates/tag-item.html',
                'templates/auto-complete.html',
                'templates/auto-complete-match.html'
            ],
            out: 'tmp/templates.js'
        }
    }
};